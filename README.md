## Introduction

Backups all opened in OneNote notebooks into PDF files

Although running the OneNote application is not required I would recommend to do so

The API creates some additional process that seems prevent the OneNote application starting

## Configuration

`basedir` key specifies base directory for the backup

System creates `OneNoteBackup` directory in the `basedir` automatically

## Dependency Injection

The Tools for Office API is encapsulated and injected into main application

Though it is possible to plug in some alternative implementation (say via direct XML manipulation)

But it is another story
