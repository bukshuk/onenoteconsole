﻿using onenoteconsole.lib.interfaces;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;

namespace onenoteconsole
{
    class Program
    {
        public static Application App { get; private set; }

        static Program()
        {
            CompositionRoot();
        }

        static void Main(string[] args)
        {
            try
            {
                App.Backup();

                Console.WriteLine();
                Console.WriteLine("Success.");
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine($"Cannot backup as: [{ex.Message}]!");
            }
            finally
            {
                Thread.Sleep(5000);
            }
        }

        static void CompositionRoot()
        {
            App = new Application(CreateInstance<IOneNoteApplication>(), GetBaseDirectoryPath());
        }

        static T CreateInstance<T>()
        {
            string key = typeof(T).Name.ToLower();

            string value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ApplicationException($"Cannot find [{key}] configuaration");
            }

            try
            {
                var assemblyNameAndTypeName = value.Split(new [] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                string assemblyName = assemblyNameAndTypeName[0];
                string typeName = assemblyNameAndTypeName[1];

                var assemblyDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var assemblyPath = Path.Combine(assemblyDirectory, assemblyName);
                var assembly = Assembly.LoadFrom(assemblyPath);
                var type = assembly.GetType(typeName, throwOnError: true);

                return (T)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Cannot create instance as: [{ex.Message}]", ex);
            }
        }

        static string GetBaseDirectoryPath()
        {
            string baseDirPath = ConfigurationManager.AppSettings["basedir"];

            if (string.IsNullOrWhiteSpace(baseDirPath) || (Directory.Exists(baseDirPath) == false))
            {
                baseDirPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            }

            return baseDirPath;
        }
    }
}
