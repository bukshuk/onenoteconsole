﻿using onenoteconsole.lib.interfaces;
using System;
using System.IO;

namespace onenoteconsole
{
    public class Application
    {
        private IOneNoteApplication OneNoteApp { get; set; }
        private string BaseDirectoryPath { get; set; }

        public Application(IOneNoteApplication oneNoteApp, string baseDirectoryPath)
        {
            if ((oneNoteApp == null) || string.IsNullOrWhiteSpace(baseDirectoryPath))
            {
                throw new ArgumentNullException();
            }

            this.OneNoteApp = oneNoteApp;
            this.BaseDirectoryPath = baseDirectoryPath;

            this.OneNoteApp.NotebookSavingStart += (s, e) => Console.Write($"Saving [{e.Value,-15}] ..... ");
            this.OneNoteApp.NotebookSavingFinish += (s, e) => Console.WriteLine("done.");
        }

        public void Backup()
        {
            string backupDirPath = GetBackupDirectoryPath();

            if (Directory.Exists(backupDirPath) == false)
            {
                Directory.CreateDirectory(backupDirPath);
            }

            this.OneNoteApp.Backup(name => Path.Combine(backupDirPath, $"{name}_{DateTime.Now.ToString("yyyy-MM-dd-HHmmss")}.pdf"));
        }

        private string GetBackupDirectoryPath()
        {
            return Path.Combine(this.BaseDirectoryPath, "OneNoteBackup");
        }
    }
}
