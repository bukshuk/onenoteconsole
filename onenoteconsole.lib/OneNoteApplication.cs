﻿using onenoteconsole.lib.interfaces;
using System;
using System.Linq;
using System.Xml.Linq;
using OneNote = Microsoft.Office.Interop.OneNote; // HACK: Set the Embed Interop Types to false (see the assembly properties in References)

namespace onenoteconsole.lib
{
    public class OneNoteApplication : IOneNoteApplication
    {
        public event EventHandler<EventArgs<string>> NotebookSavingStart;
        public event EventHandler<EventArgs<string>> NotebookSavingFinish;

        public void Backup(Func<string, string> fileNamePredicate)
        {
            var oneNoteApp = new OneNote.Application();

            oneNoteApp.GetHierarchy(null, OneNote.HierarchyScope.hsNotebooks, out string oneNoteAppHierarchy);

            var xdNotebooks = XDocument.Parse(oneNoteAppHierarchy);
            var xdNamespace = xdNotebooks.Root.Name.Namespace;

            foreach (var notebook in xdNotebooks.Descendants(xdNamespace + "Notebook").Select(n => new
            {
                Id = n.Attribute("ID").Value,
                Name = n.Attribute("name").Value
            }))
            {
                NotebookSavingStart?.Invoke(this, new EventArgs<string>(notebook.Name));
                
                oneNoteApp.Publish(notebook.Id, fileNamePredicate.Invoke(notebook.Name), OneNote.PublishFormat.pfPDF);

                NotebookSavingFinish?.Invoke(this, new EventArgs<string>(notebook.Name));
            }
        }
    }
}

