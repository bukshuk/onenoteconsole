﻿using onenoteconsole.lib.interfaces;
using System;

namespace onenoteconsole.test
{
    class OneNoteApplicationUt : IOneNoteApplication
    {
        public event EventHandler<EventArgs<string>> NotebookSavingStart;
        public event EventHandler<EventArgs<string>> NotebookSavingFinish;

        public void Backup(Func<string, string> fileNamePredicate)
        {
            NotebookSavingStart?.Invoke(this, new EventArgs<string>("UnitTest"));

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Hello from SMOKE test!");
            Console.WriteLine();

            NotebookSavingFinish?.Invoke(this, new EventArgs<string>(null));
        }
    }
}
