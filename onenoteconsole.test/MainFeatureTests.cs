﻿using NUnit.Framework;
using utest.net;

namespace onenoteconsole.test
{
    public class MainFeatureTests : TestBase
    {
        [Test]
        public void SmokeTest()
        {
            var app = new onenoteconsole.Application(new OneNoteApplicationUt(), this.TestOutputDirectory);

            app.Backup();
        }
    }
}
