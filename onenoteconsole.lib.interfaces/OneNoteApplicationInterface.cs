﻿using System;

namespace onenoteconsole.lib.interfaces
{
    public interface IOneNoteApplication
    {
        event EventHandler<EventArgs<string>> NotebookSavingStart;
        event EventHandler<EventArgs<string>> NotebookSavingFinish;

        void Backup(Func<string, string> fileNamePredicate);
    }
}
